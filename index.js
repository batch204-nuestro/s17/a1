/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	function userData() {
		let fullName = prompt("enter full name:")
		let age = prompt("enter age:");
		let location = prompt("enter location");

		console.log("Hello, " + fullName);
		console.log("You are " + age + " years old.");
		console.log("You live in " + location);
	}

	userData();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	function favBands() {
		let band1 = "Paramore";
		let band2 = "The Beatles";
		let band3 = "Animals as Leaders";
		let band4 = "Dream Theater"
		let band5 = "Incubus";

		console.log("1. " + band1);
		console.log("2. " + band2);
		console.log("3. " + band3);
		console.log("4. " + band4);
		console.log("5. " + band5);
	}
	favBands();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	function favMovie() {
		let favMovie1 = "John wick Chapter 1";
		let favMovie2 = "Sword Art Online";
		let favMovie3 = "Parasite";
		let favMovie4 = "Narnia";
		let favMovie5 = "Taken";

		console.log("1. " + favMovie1);
		console.log("Rotten Tomatoes Rating: 90%");
		console.log("2. " + favMovie2);
		console.log("Rotten Tomatoes Rating: 89%");
		console.log("3. " + favMovie3);
		console.log("Rotten Tomatoes Rating: 88%");
		console.log("4. " + favMovie4);
		console.log("Rotten Tomatoes Rating: 85%");
		console.log("5. " + favMovie5);
		console.log("Rotten Tomatoes Rating: 91%");
	}
favMovie();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};


printUsers();